# ElK Stack 

The ELK stack stands for ElasticSearch, Logstash and Kibana stack. Let's look at each technology individually:

- ElasticSearch is a super fast, key:value DB. Part of the NoSQL family
- Logstash is an agent/retreaver of logs in the machine/app you want to monitor
- Kiabana is a data visualization tool. It does not store data, but it makes graphs and other.


The combination of these 3 technologies helps you monitor your:

-  App
-  The infrastructure
-  Traffic


This is important section of Devops. Monitoring.

Look at the diagram to understand what we are setting up: 




-----------Diagram ----------


#### Notes for resilence

If you want to access your data fast and make sure you don't loose data you usually setup a ElasticSearch cluster. Once again this is more expensive, but ensure resilience and fast access. 


## setting up and ELK stack

First we need a ElasticSearch DB, then well Kibana. 

Let's see. ElasticSearch needs:
- 8 cpu
- 16gb ram

We're probably get away with using a t3a.xlarge.

Then we need something to run Kibana.

Kibana has no specific mininum requierment as it's not where the data is stored.

Let's set these up.

### ElasticSearch setup 

```
$ sudo yum install elasticsearch
# https://www.elastic.co/downloads/elasticsearch
# https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.13.1-x86_64.rpm

$ sudo yum -y install https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.13.1-x86_64.rpm

$ sudo su

$ cd /etc/elasticsearch

$ nano elastisearch.yml

```

Inside `elastisearch.yml`

```
# edit the cluster name
# this is so each of your db is in a seperate cluster!
# but if you where setting up a cluster you would put 2 or 3 machines with the same name

## path/data 
# this is where all the important stuff is kept
# remove this if you want to start fresh or import if you want to save

## http.port
# important bucause this is where you point kibana


##cluster.initial_master_node: ["node-1"]

```

Let's start the service

```
$ systemctl start elasticsearch
$ systemctl status
$ less /var/log/messages 
# shit + g
```

```
$ netstat -tln
```